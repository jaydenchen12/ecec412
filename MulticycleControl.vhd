library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity MulticycleControl is

port(Opcode:in std_logic_vector(5 downto 0);
     clk:in std_logic;
     RegDst,RegWrite,ALUSrcA,IRWrite,MemtoReg,MemWrite,MemRead,IorD,PCWrite,PCWriteCond:out std_logic;
     ALUSrcB,ALUOp,PCSource:out std_logic_vector(1 downto 0));
end MulticycleControl;

architecture Behavioral of MulticycleControl is
--variables here
begin--begin architecture
process(Opcode, clk)
variable state: integer:= 1;
begin--begin process
if clk='1' and clk'event then
	case Opcode is
	when "000000"=>--R-type
	  case state is
	  when 1=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='1';MemtoReg<='0';MemWrite<='0';MemRead<='1';IorD<='0';PCWrite<='1';PCWriteCond<='0';
	    ALUSrcB<="01";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 2=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="11";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 3=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='1';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="00";ALUOp<="10";PCSource<="00";
	    state:= state + 1;
	  when 4=>
	    RegDst<='1';RegWrite<='1';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="01";ALUOp<="00";PCSource<="00";
	    state:= 1;
	  when others=>
	  end case;
	when "000100"=>--beq
	  case state is
	  when 1=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='1';MemtoReg<='0';MemWrite<='0';MemRead<='1';IorD<='0';PCWrite<='1';PCWriteCond<='0';
	    ALUSrcB<="01";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 2=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="11";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 3=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='1';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='1';
	    ALUSrcB<="00";ALUOp<="01";PCSource<="01";
	    state:= 1;
	  when others=>
	  end case;
	when "000010"=>--jump
	  case state is
	  when 1=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='1';MemtoReg<='0';MemWrite<='0';MemRead<='1';IorD<='0';PCWrite<='1';PCWriteCond<='0';
	    ALUSrcB<="01";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 2=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="11";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 3=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='1';PCWriteCond<='0';
	    ALUSrcB<="11";ALUOp<="00";PCSource<="10";
	    state:= 1;
	  when others=>
	  end case;
	when "100011"=>--lw
	  case state is
	  when 1=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='1';MemtoReg<='0';MemWrite<='0';MemRead<='1';IorD<='0';PCWrite<='1';PCWriteCond<='0';
	    ALUSrcB<="01";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 2=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="11";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 3=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='1';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="10";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 4=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='1';IorD<='1';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="01";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 5=>
	    RegDst<='0';RegWrite<='1';ALUSrcA<='0';IRWrite<='0';MemtoReg<='1';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="01";ALUOp<="00";PCSource<="00";
	    state:= 1;
	  when others=>
	  end case;
	when "101011"=>--sw
	  case state is
	  when 1=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='1';MemtoReg<='0';MemWrite<='0';MemRead<='1';IorD<='0';PCWrite<='1';PCWriteCond<='0';
	    ALUSrcB<="01";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 2=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="11";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 3=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='1';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="10";ALUOp<="00";PCSource<="00";
	    state:= state + 1;
	  when 4=>
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='1';MemRead<='0';IorD<='1';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="01";ALUOp<="00";PCSource<="00";
	    state:= 1;
	  when others=>
	  end case;
	when others=>--all other func codes
	    RegDst<='0';RegWrite<='0';ALUSrcA<='0';IRWrite<='0';MemtoReg<='0';MemWrite<='0';MemRead<='0';IorD<='0';PCWrite<='0';PCWriteCond<='0';
	    ALUSrcB<="00";ALUOp<="00";PCSource<="00";
	end case;

end if;
end process;
end Behavioral;
