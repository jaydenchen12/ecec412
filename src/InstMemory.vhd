library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity InstMemory is
generic(n:natural:=32);
port(Address:in std_logic_vector(n-1 downto 0);
     ReadData:out std_logic_vector(n-1 downto 0));
end InstMemory;

architecture behavioral of InstMemory is
	type byte is array (natural range <>) of std_logic_vector(7 downto 0);
	signal inst : byte(n-1 downto 0) := (others => "00000000");
begin

-- Put instructions here. 4 entries per instruction.
inst(0)  <= "10001101"; inst(1)  <= "00010101"; inst(2)  <= "00000000"; inst(3)  <= "00000000";  -- lw $s5, 0($t0)
inst(4)  <= "10001101"; inst(5)  <= "00010110"; inst(6)  <= "00000000"; inst(7)  <= "00000100";  -- lw $s6, 4($t0)
inst(8)  <= "00000010"; inst(9)  <= "10110110"; inst(10) <= "01111000"; inst(11) <= "00101010";  -- slt $t7, $s5, $s6
inst(12) <= "00010001"; inst(13) <= "11100000"; inst(14) <= "00000000"; inst(15) <= "00000000";  -- beq $t7, $zero
inst(16) <= "00000010"; inst(17) <= "01010011"; inst(18) <= "10001000"; inst(19) <= "00100010";  -- sub $s1,$s2,$s3 
inst(20) <= "00001000"; inst(21) <= "00000000"; inst(22) <= "00000000"; inst(23) <= "00000111";  -- j exit
inst(24) <= "00000010"; inst(25) <= "01010011"; inst(26) <= "10001000"; inst(27) <= "00100000";  -- L: add $s1,$s2,$s3
inst(28) <= "10101101"; inst(29) <= "00010001"; inst(30) <= "00000000"; inst(31) <= "00001100";  -- exit: sw $s1, 12($t0)

-- $t0-$t7 [ 8-15]
-- $s0-$s7 [16-23]
-- $t8-$t9 [24-25]

ReadData <= inst(to_integer(unsigned(Address)))
	&inst(to_integer(unsigned(Address))+1)
	&inst(to_integer(unsigned(Address))+2)
	&inst(to_integer(unsigned(Address))+3);

end behavioral;

