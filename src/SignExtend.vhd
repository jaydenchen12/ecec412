library ieee;
use ieee.std_logic_1164.all;

entity SignExtend is
port(
x:in std_logic_vector(15 downto 0);
y:out std_logic_vector(31 downto 0));
end SignExtend;


architecture beh of SignExtend is

begin
process(x)
begin
	if x(15) = '0' then
		y <= "0000000000000000" & x;
	else 
		y <= "1111111111111111" & x;
	end if;
end process;
end beh;

