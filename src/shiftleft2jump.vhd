library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity shifLeft2Jump is
port(x: in  std_logic_vector(25 downto 0);
     y: in  std_logic_vector(3 downto 0);
     z: out std_logic_vector(31 downto 0));

end shifLeft2Jump;

architecture Beh of shifLeft2Jump is

begin

z <= y&x&"00";

end beh;