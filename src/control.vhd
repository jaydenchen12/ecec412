library ieee;
use ieee.std_logic_1164.all;

entity Control is
port(Opcode:in std_logic_vector(5 downto 0);
     RegDst,Branch,MemRead,MemtoReg,MemWrite,ALUSrc,RegWrite,Jump:out std_logic;
     ALUOp:out std_logic_vector(1 downto 0));
end Control;

architecture behavioral of control is
begin
	process(Opcode)
	begin
		-- RegDst & ALUOp1
		if Opcode = "000000" then
			RegDst <= '1';
			ALUOp(1) <= '1';
		else 
			RegDst <= '0';
			ALUOp(1) <= '0';
		end if;
		-- Branch & ALUOp0
		if Opcode = "000100" then
			Branch <= '1';
			ALUOp(0) <= '1';
		else 
			Branch <= '0';
			ALUOp(0) <= '0';
		end if;
		-- MemRead & MemtoReg
		if Opcode = "100011" then
			MemRead <= '1';
			MemtoReg <= '1';
		else 
			MemRead <= '0';
			MemtoReg <= '0';
		end if;
		-- MemWrite
		if Opcode = "101011" then
			MemWrite <= '1';
		else 
			MemWrite <= '0';
		end if;
		-- ALUSrc
		if (Opcode = "100011" or Opcode = "101011") then
			ALUSrc <= '1';
		else 
			ALUSrc <= '0';
		end if;
		-- RegWrite
		if (Opcode = "100011" or Opcode = "000000") then
			RegWrite <= '1';
		else 
			RegWrite <= '0';
		end if;
		-- Jump
		if Opcode = "000010" then
			Jump <= '1';
		else Jump <= '0';
		end if;
	end process;
end behavioral;