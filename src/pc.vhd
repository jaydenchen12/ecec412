----------------------------------------------------------------------------------
-- Nicholas Kachur
-- ECEC 355
-- 2016-01-06
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity PC is
	port ( clk : in std_logic;
		   AddressIn : in std_logic_vector(31 downto 0);
		   AddressOut : out std_logic_vector(31 downto 0) );
end PC;

architecture Behavioral of PC is
signal temp : std_logic_vector(31 downto 0) := (others => '0');
begin

	

	process(clk)
	variable count : integer := 0;
	begin
		if clk='1' and clk'event then
			--temp <= AddressIn;
			if count=0 then
				AddressOut <= (others => '0');
				count := count + 1;
			else
				AddressOut <= AddressIn;
			end if;
		end if;
	end process;

end Behavioral;