library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity NOR32 is
port( x:in std_logic_vector(31 downto 0);
	  y:out std_logic);
end NOR32;

architecture Behavioral of NOR32 is

begin

process(x)
begin
if x = "00000000000000000000000000000000" then y <= '1'; else y <= '0'; end if;
end process;

end Behavioral;
