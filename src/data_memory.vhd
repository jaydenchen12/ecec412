library ieee;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity DataMemory is
generic (N: natural := 32);
port(	WriteData:in std_logic_vector(31 downto 0);
     	Address:in std_logic_vector(31 downto 0);
     	Clk,MemRead,MemWrite:in std_logic;
     	ReadData:out std_logic_vector(31 downto 0));
end DataMemory;

architecture beh of DataMemory is

type mem_locations is array(N downto 0) of std_logic_vector(7 downto 0);
signal memory: mem_locations := (7 => (2 => '1', others => '0'),
				11 => (2 => '1', 0 =>'1', others => '0'),
				others => (others => '0'));

begin

process(clk)
begin
--if clk='1' and clk'event then
if clk='0' and clk'event then
	if MemRead='1' then
		--ReadData <= (others =>'1');
		ReadData <= memory(conv_integer(Address))&
				memory(conv_integer((Address))+1)&
				memory(conv_integer((Address))+2)&
				memory(conv_integer((Address))+3);
	end if;
end if;

if clk='1' and clk'event then
	if MemWrite='1' then
		memory(conv_integer((Address))) <= WriteData(31 downto 24);
		memory(conv_integer((Address))+1) <= WriteData(23 downto 16);
		memory(conv_integer((Address))+2) <= WriteData(15 downto 8);
		memory(conv_integer((Address))+3) <= WriteData(7 downto 0); 
	end if;
end if;
end process;

--sReadData <= temp;
end beh;
