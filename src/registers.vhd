-----------------------------------------------------------------------
-- Author: Nicholas Kachur
-- Course: ECEC 412
-- Date Modified: 2017-10-02
-----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity registers is
	port ( RR1, RR2, WR : in std_logic_vector(4 downto 0);
	       WD : in std_logic_vector(31 downto 0);
	       Clk, RegWrite : in std_logic;
	       RD1, RD2 : out std_logic_vector(31 downto 0) );
end registers;

architecture Behavioral of registers is

type register_array is array(31 downto 0) of std_logic_vector(31 downto 0);
signal registers : register_array := (0 => (others => '0'),
				      8 => ("00000000000000000000000000000100"),
				      18 => ("00000000000000000000000000001101"),
				      19 => ("00000000000000000000000000000100"),
				      others => (others => '0'));

begin

process(Clk, RR1, RR2, WR, WD, RegWrite)
begin
	if Clk='0' and Clk'event then
		RD1 <= registers(((conv_integer(RR1))));
		RD2 <= registers((conv_integer(RR2)));
		
	end if;

	if Clk='1' and Clk'event then
		if (RegWrite = '1') and (WR /= "00000") then
			registers((conv_integer(WR))) <= WD;
		end if;
	end if;
end process;

end Behavioral;

