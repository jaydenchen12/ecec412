library IEEE;
use IEEE.std_logic_1164.all;

entity MUX5 is
port(x,y:in std_logic_vector (4 downto 0);
     sel:in std_logic;
     z:out std_logic_vector(4 downto 0));
end MUX5;

architecture Behavioral of MUX5 is
component mux
	generic(N: natural);
	port(x,y:in std_logic_vector (N downto 0);
	     sel:in std_logic;
 	     z:out std_logic_vector(N downto 0));
end component;

begin
G1: mux generic map(4)
	port map(x,y,sel,z);
end Behavioral;
