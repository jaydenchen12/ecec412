library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity alu is
generic(n:natural:=32);
port(a,b:in std_logic_vector(n-1 downto 0);
	  Oper:in std_logic_vector(3 downto 0);
	  Result:buffer std_logic_vector(n-1 downto 0);
	  Zero,Overflow:buffer std_logic);

end alu;

architecture Behavioral of alu is 
component onebitALU is
port(
	a,b,less:in std_logic;
	ainv, binv: in std_logic;
	oper: in std_logic_vector(1 downto 0);
	cin: in std_logic;
	inZero: in std_logic;
	-- Output
	cout: buffer std_logic;
	set: buffer std_logic;
	result: buffer std_logic;
	Overflow: buffer std_logic;
	outZero: buffer std_logic);
end component;

type one_d is array (natural range <>) of std_logic;
signal cout_arr : one_d(n-2 downto 0);
signal isZero : std_logic_vector(n-2 downto 0);
signal set: std_logic :='0';
begin

U1: for i in 0 to n-1 generate
	U2: if i=0 generate
		G1: onebitALU port map(a(i), b(i), set, Oper(3), Oper(2), Oper(1 downto 0), 
			Oper(2), '1', cout_arr(i), open, Result(i), open, isZero(i));
	end generate U2;

	U4: if i/=0 and i/=(n-1) generate
		G3: onebitALU port map(a(i), b(i), '0', Oper(3), Oper(2), Oper(1 downto 0), 
			cout_arr(i-1), isZero(i-1), cout_arr(i), open, Result(i), open, isZero(i));
	end generate U4;

	U5: if i=(n-1) generate
		G4: onebitALU port map(a(i), b(i), '0', Oper(3), Oper(2), Oper(1 downto 0), 
			cout_arr(i-1), isZero(i-1), open, set, Result(i), Overflow, Zero);
		
	end generate U5;
end generate U1;

end Behavioral;
