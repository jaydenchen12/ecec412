
library ieee;
use ieee.std_logic_1164.all;

entity onebitALU is
port(
	a,b,less:in std_logic;
	ainv, binv: in std_logic;
	oper: in std_logic_vector(1 downto 0);
	cin: in std_logic;
	inZero: in std_logic;
	-- Output
	cout: buffer std_logic;
	set: buffer std_logic;
	result: buffer std_logic;
	Overflow: buffer std_logic;
	outZero: buffer std_logic);
end onebitALU ;

architecture Behavioral of onebitALU is 
begin

process(a, b, less, ainv, binv, oper, cin, inZero)
variable aOut : std_logic;
variable bOut : std_logic;
variable r:std_logic;
variable t:std_logic;
begin
	-- Set The State from operation
	if ainv='1' then
		aOut := not a;
	else
		aOut := a;
	end if;

	-- Set the state from operation
	if binv='1' then
		bOut := not b;
	else
		bOut := b;
	end if;

	case oper is 
		when "00" => -- And
			r := aOut and bOut;
			cout <= '0';
			set <= '0';
			Overflow <= '0';
		when "01" => -- OR
			r := aOut or bOut;
			set <='0';
			Overflow <= '0';
		when "10" => -- Add
			r := (aOut xor bOut) xor cin;
			set <= '0';
			if aOut='1' and bOut='1' and r='0' then
				Overflow <= '1';
			elsif aOut='0' and bOut='0' and r='1' then
				Overflow <= '1';
			else
				Overflow <='0';
			end if;
		when "11" => -- Spit out Same results
			r := less;
			t := (aOut xor bOut) xor cin;
			if aOut='1' and bOut='1' and t='0' then
				Overflow <= '1';
				set <= '0';
			elsif aOut='0' and bOut='0' and t='1' then
				Overflow <= '1';
				set <= '0';
			else
				Overflow <='0';
				set <= (aOut xor bOut) xor cin; 
			end if;
		when others =>null;
	end case;
	result <= r;
	cout <= (aOut and bOut) or (cin and (aOut xor bOut));
	outZero <= inZero and not(r);
end process;

end Behavioral;
