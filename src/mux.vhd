library IEEE;
use IEEE.std_logic_1164.all;

entity mux is
generic(N: natural);
	port(x,y:in std_logic_vector (N downto 0);
	     sel:in std_logic;
 	     z:out std_logic_vector(N downto 0));
end mux;

architecture Behavioral of mux is
begin
z <= x when (sel='0') else y;
end Behavioral;