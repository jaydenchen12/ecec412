------------------------------------------------------------------------
-- Authors: Yeoh Chan, Fritz Dodoo, Nicholas Kachur, Kevin Rosenberg
-- Course: ECEC 355
-- Date Modified: 2016-02-20
------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALUControl is
    port ( ALUOp : in std_logic_vector(1 downto 0);
           Funct : in std_logic_vector(5 downto 0);
           Operation : out std_logic_vector(3 downto 0) );
end ALUControl;

architecture Behavioral of ALUControl is
begin
    
process(ALUOp, Funct)
begin
    case ALUOp is
    when "00" =>
        Operation <= "0010";
    when "01" =>
        Operation <= "0110";
    when "10" =>

        case Funct is
        when "100100" =>
            Operation <= "0000";
        when "100101" =>
            Operation <= "0001";
        when "100000" =>
            Operation <= "0010";
        when "100010" =>
            Operation <= "0110";
        when "101010" =>
            Operation <= "0111";
        when others =>
            null;
        end case;

    when others =>
        null;
    end case;
end process;


end Behavioral;

