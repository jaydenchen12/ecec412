library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ShiftLeft2 is
Port ( x : in std_logic_vector(31 downto 0);
       y : out std_logic_vector(31 downto 0));
end ShiftLeft2;

architecture Behavioral of ShiftLeft2 is

begin

y <= x(29 downto 0)&x(31 downto 30);

end Behavioral;
