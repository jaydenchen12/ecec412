library IEEE;
use IEEE.std_logic_1164.all;

entity MUX32 is
port(x,y:in std_logic_vector (31 downto 0);
     sel:in std_logic;
     z:out std_logic_vector(31 downto 0));
end MUX32;

architecture Behavioral of MUX32 is
component mux
	generic(N: natural);
	port(x,y:in std_logic_vector (N downto 0);
	     sel:in std_logic;
 	     z:out std_logic_vector(N downto 0));
end component;

begin
G1: mux generic map(31)
	port map(x,y,sel,z);
end Behavioral;
