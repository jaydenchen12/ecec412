-----------------------------------------------------------------------
-- Authors: Yeoh Chan, Fritz Dodoo, Nicholas Kachur, & Kevin Rosenberg
-- Course: ECEC 355
-- Date Modified: 2016-03-02
-----------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity CPU is
port(clk:in std_logic;
Overflow: out std_logic);
end entity;


architecture Structural of CPU is
---- Instruction Memory -----
component InstMemory is
generic(n:natural:=32);
port(Address:in std_logic_vector(n-1 downto 0);
     ReadData:out std_logic_vector(n-1 downto 0));
end component;

----- Multiplexer 5 -------
component MUX5 is
port(x,y:in std_logic_vector (4 downto 0);
     sel:in std_logic;
     z:out std_logic_vector(4 downto 0));
end component;

--------- Registers --------
component registers is
	port ( RR1, RR2, WR : in std_logic_vector(4 downto 0);
	       WD : in std_logic_vector(31 downto 0);
	       Clk, RegWrite : in std_logic;
	       RD1, RD2 : out std_logic_vector(31 downto 0) );
end component;

---------- Sign Extend ------
component SignExtend is
port(
x:in std_logic_vector(15 downto 0);
y:out std_logic_vector(31 downto 0));
end component;

----- Control ------
component Control is
port(Opcode:in std_logic_vector(5 downto 0);
     RegDst,Branch,MemRead,MemtoReg,MemWrite,ALUSrc,RegWrite,Jump:out std_logic;
     ALUOp:out std_logic_vector(1 downto 0));
end component;

----- ALU Control ------
component ALUControl is
    port ( ALUOp : in std_logic_vector(1 downto 0);
           Funct : in std_logic_vector(5 downto 0);
           Operation : out std_logic_vector(3 downto 0) );
end component;

------ MUX 32 ------
component MUX32 is
port(x,y:in std_logic_vector (31 downto 0);
     sel:in std_logic;
     z:out std_logic_vector(31 downto 0));
end component;

-------- ALU -----
component alu is
generic(n:natural:=32);
port(a,b:in std_logic_vector(n-1 downto 0);
	  Oper:in std_logic_vector(3 downto 0);
	  Result:buffer std_logic_vector(n-1 downto 0);
	  Zero,Overflow:buffer std_logic);

end component;

------ Data Memory -------
component DataMemory is
generic (N: natural := 32);
port(	WriteData:in std_logic_vector(31 downto 0);
     	Address:in std_logic_vector(31 downto 0);
     	Clk,MemRead,MemWrite:in std_logic;
     	ReadData:out std_logic_vector(31 downto 0));
end component;

----- AND 2 ------
component AND2 is
port(x,y:in std_logic;z:out std_logic);
end component;

------- PC --------
component PC is
	port ( clk : in std_logic;
		   AddressIn : in std_logic_vector(31 downto 0);
		   AddressOut : out std_logic_vector(31 downto 0) );
end component;

------- Shift Left 2 ----
component ShiftLeft2 is
Port ( x : in std_logic_vector(31 downto 0);
       y : out std_logic_vector(31 downto 0));
end component;

-------- Shift Left 2 Jump -----
component shifLeft2Jump is
port(x: in  std_logic_vector(25 downto 0);
     y: in  std_logic_vector(3 downto 0);
     z: out std_logic_vector(31 downto 0));
end component;

------ Signals ---------

signal A, C, D, E, F, G, H, J, K, L, M, N, P, Q : std_logic_vector(31 downto 0);
signal B : std_logic_vector(4 downto 0);
signal R : std_logic;
signal RegDst, Branch, MemRead, MemtoReg, MemWrite, ALUSrc, RegWrite, Jump : std_logic;
signal ALUOp : std_logic_vector(1 downto 0);
signal Instruction : std_logic_vector(31 downto 0); -- The output of Instruction Memory
signal Operation : std_logic_vector(3 downto 0);
signal Zero: std_logic;

 
begin
	-- Instruction Fetch Stage
	PC_1: PC port map(clk, P, A);
	IM_1: InstMemory port map(A, Instruction);
	CONTROL_1: Control port map(Instruction(31 downto 26), RegDst, Branch, MemRead, MemtoReg, MemWrite, ALUSrc, RegWrite, Jump, ALUOp);
	ALU_CONTROL_1: ALUControl port map(ALUOp, Instruction(5 downto 0), Operation);
	
	-- Instruction Decode Stage
	MUX5_B: MUX5 port map(Instruction(20 downto 16), Instruction(15 downto 11), RegDst, B);
	REGISTER_1: registers port map(Instruction(25 downto 21), Instruction(20 downto 16), B, J, clk, RegWrite, C, D);
	SIGN_EXTEND_1: SignExtend port map(Instruction(15 downto 0), E);
	MUX32_F: MUX32 port map(D, E, ALUSrc, F);
	ALU_1: alu port map(C, F, Operation, G, Zero, Overflow);
	DM_1: DataMemory port map(D, G, clk, MemRead, MemWrite, H);
	MUX32_J: MUX32 port map(G, H, MemtoReg, J);


	ADD4_1: alu port map(A, (2=>'1', others=>'0'), "0010", L, open, open);
	
	SHIFT_LEFT_2_JUMP_1: shifLeft2Jump port map(Instruction(25 downto 0), L(31 downto 28), Q);
	SHIFT_LEFT_2_1: ShiftLeft2 port map(E, K);
	-- Execute Stage
	-- Memory Stage
	BRANCH_ADDER_1: alu port map(L, K, "0010", M, open, open);
	AND2_1: AND2 port map(Branch, Zero, R);
	MUX32_N: MUX32 port map(L, M, R, N);
	MUX32_P: MUX32 port map(N, Q, Jump, P);
	-- Write-Back Stage
end Structural;